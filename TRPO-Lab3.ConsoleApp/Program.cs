using System;
using library;


namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {


                Console.WriteLine("Введите внешний радиус");
                double OutR = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите внутренный радиус");
                double InR = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введите угол который соответствует необходимому сектору");
                double a = Convert.ToDouble(Console.ReadLine());
                if (OutR < InR)
                {
                    Console.WriteLine("Внешний радиус не может быть меньше внутренненго!");
                    throw new Exception();
                }
                else
                {
                    double s = Class1.GetSq(OutR, InR, a);
                    Console.WriteLine($"Площадь сектора кольца = {s} ед.изм.");
                }
            }
            catch
            {
                Main(args);
            }
            Console.ReadKey();
        }
    }
}
