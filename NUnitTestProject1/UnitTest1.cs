using library;
using NUnit.Framework;
using System;

namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            const double i = 164.15;
            Double S = Class1.GetSq(15, 4, 90);
            Assert.AreEqual(i, S);

        }
        [Test]
        public void Testing()
        {
            Assert.Throws<ArgumentException>(() => Class1.GetSq(-15, 4, -90));
        }

    }
}